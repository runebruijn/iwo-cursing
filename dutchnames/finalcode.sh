#usr/bin/bash

echo "AANTAL MANNELIJKE TWEETS:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102\:*.out.gz  | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'male' | wc -l


echo "AANTAL VROUWELIJKE TWEETS:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102\:*.out.gz  | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'female' | wc -l


echo "AANTAL MANNELIJKE TWEETS WAARIN GESCHOLDEN WORDT:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102\:*.out.gz  | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'male' | grep -i -e 'klootzak' -e 'idioot' -e 'flikker' -e 'bitch' -e 'kutwijf' -e 'sukkel' -e 'teringlijer' -e 'tyfuslijer' -e 'kankerlijer' -e 'teringzooi' -e 'tyfuszooi' -e  'kankerzooi' -e  'shit' -e  'godver' -e 'gvd' -e 'kut' -e 'fuck' -e 'zeikerd' -e 'mafkees' | wc -l


echo "AANTAL VROUWELIJKE TWEETS WAARIN GESCHOLDEN WORDT:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102\:*.out.gz  | /net/corpora/twitter2/tools/tweet2tab user text | python get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'female' | grep -i -e 'klootzak' -e 'idioot' -e 'flikker' -e 'bitch' -e 'kutwijf' -e 'sukkel' -e 'teringlijer' -e 'tyfuslijer' -e 'kankerlijer' -e 'teringzooi' -e 'tyfuszooi' -e  'kankerzooi' -e  'shit' -e  'godver' -e 'gvd' -e 'kut' -e 'fuck' -e 'zeikerd' -e 'mafkees' | wc -l


echo "50 MANNELIJKE TWITTER NAMEN OM TE CONTROLEREN:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i user | python get-gender.py | grep -v 'RT' | grep -v '@' | sort -r | uniq | head -n 50


echo "50 VROUWELIJKE TWITTER NAMEN OM TE CONTROLEREN:"
zless /net/corpora/twitter2/Tweets/2017/01/20170102\:12.out.gz  | /net/corpora/twitter2/tools/tweet2tab -i user | python get-gender.py | grep -v 'RT' | grep -v '@' | sort | uniq | head -n 50
